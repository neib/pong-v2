import pygame
import random

# Get screen resolution
pygame.init()
infoRes = pygame.display.Info()
pygame.quit()
# Fullscreen
WIDTH = infoRes.current_w
HEIGHT = infoRes.current_h
# Manual set
#WIDTH = 1024
#HEIGHT = 768


# ---------------------------------------------------------------------------- #
# Bonus
# ---------------------------------------------------------------------------- #
class Bonus():
    def __init__(self):
        self.bonusImg = pygame.image
        self.sizeUpImg = pygame.image.load("images/sizeUp.png")
        self.sizeDownImg = pygame.image.load("images/sizeDown.png")
        self.speedImg = pygame.image.load("images/speed.png")
        self.switchImg = pygame.image.load("images/switch.png")
        #self.splitImg = pygame.image.load("images/split.png")
        #self.smashImg = pygame.image.load("images/smash.png")

        self.bonusX = WIDTH/2-15
        self.bonusDirX = 0
        self.bonusY = random.choice((HEIGHT/4-15,HEIGHT/2-15,3*HEIGHT/4-15))

    # ------------------------------------------------------------------------ #
    # New Bonus
    # ------------------------------------------------------------------------ #
    def initBonus(self):
        # Choose bonus
        self.bonus = random.choice(("up","down","speed","switch"))#,"split","smash"))
        #self.bonus = "split"
        # Choose a direction
        bonusDirection = random.choice(("left","right"))
        if bonusDirection == "left":
            self.bonusDirX = -3.5
        elif bonusDirection == "right":
            self.bonusDirX = 3.5
        # Put the corresponding image
        if self.bonus == "up":
            self.bonusImg = self.sizeUpImg
        elif self.bonus == "down":
            self.bonusImg = self.sizeDownImg
        elif self.bonus == "speed":
            if bonusDirection == "left":
                self.bonusImg = pygame.transform.rotate(self.speedImg, 270)
            elif bonusDirection == "right":
                self.bonusImg = pygame.transform.rotate(self.speedImg, 90)
        elif self.bonus == "switch":
            self.bonusImg = self.switchImg
        #elif self.bonus == "split":
        #    self.bonusImg = self.splitImg
        #elif self.bonus == "smash":
        #    if bonusDirection == "left":
        #        self.bonusImg = pygame.transform.rotate(self.smashImg, 270)
        #    elif bonusDirection == "right":
        #        self.bonusImg = pygame.transform.rotate(self.smashImg, 90)
    # ------------------------------------------------------------------------ #
    # Reset
    # ------------------------------------------------------------------------ #
    def destroy(self):
        self.bonusX = WIDTH/2-15
        #self.bonusY = HEIGHT/2-15
        self.bonusY = random.choice((HEIGHT/4-15,HEIGHT/2-15,3*HEIGHT/4-15))
        self.bonusDirX = 0
    # ------------------------------------------------------------------------ #
    # Throw Bonus
    # ------------------------------------------------------------------------ #
    def new(self):
        self.bonusX += self.bonusDirX
