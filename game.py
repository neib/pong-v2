import pygame
from players import *
from ball import *
from bonus import *

# Get screen resolution
pygame.init()
infoRes = pygame.display.Info()
pygame.quit()
# Fullscreen
WIDTH = infoRes.current_w
HEIGHT = infoRes.current_h
# Manual set
#WIDTH = 1024
#HEIGHT = 768

# Init colors
BLACK = (0,0,0)
WHITE = (255,255,255)
#RED = (255,0,0)
#GREEN = (0,255,0)
#BLUE = (0,0,255)

class Game():
    def __init__(self,screen):
        self.screen = screen
        self.font = pygame.font.Font("freesansbold.ttf",32)
        self.pOne = PlayerOne()
        self.pTwo = PlayerTwo()
        self.ball = Ball()
        self.bonus = Bonus()

        self.gameOver = True
        self.firstGame = True
        # -------------------------------------------------------------------- #
        # Middle Axe
        self.middleX = WIDTH/2 -5
        self.middleSpace = 68
        # -------------------------------------------------------------------- #

        # -------------------------------------------------------------------- #
        # Bonus part
        # -------------------------------------------------------------------- #
        # Time to Spawn
        self.timer = 0
        self.goBonus = False
        self.direction = 0
        # Speed Effect
        self.speed = False
        self.velocity = 1
        self.speedBonusTime = 0
        # Switch One
        self.switchOne = False
        self.switchOneBonusTime = 0
        # Switch Two
        self.switchTwo = False
        self.switchTwoBonusTime = 0
        # Resize One
        self.upOne = False
        self.upOneBonusTime = 0
        self.upTwo = False
        self.upTwoBonusTime = 0
        # Resize Two
        self.downOne = False
        self.downOneBonusTime = 0
        self.downTwo = False
        self.downTwoBonusTime = 0

    # ------------------------------------------------------------------------ #
    # Keyboard events
    # ------------------------------------------------------------------------ #
    def events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return False

        self.key = pygame.key.get_pressed()
        # -------------------------------------------------------------------- #
        # Quit game
        # -------------------------------------------------------------------- #
        if self.key[pygame.K_ESCAPE]:
            return False
        # -------------------------------------------------------------------- #
        # Player One -> Normal input
        # -------------------------------------------------------------------- #
        if not self.switchOne:
            if self.key[pygame.K_s]:
                self.pOne.pOneMoveUp(8)
            elif self.key[pygame.K_d]:
                self.pOne.pOneMoveDown(8)
        # -------------------------------------------------------------------- #
        # Switched input (Bonus effect)
        # -------------------------------------------------------------------- #
        else:
            # Remaining time for switch effect
            self.switchOneBonusTime +=1
            if self.switchOneBonusTime == 300:
                self.switchOne = False
                self.switchOneBonusTime = 0
            # New binds
            if self.key[pygame.K_d]:
                self.pOne.pOneMoveUp(8)
            elif self.key[pygame.K_s]:
                self.pOne.pOneMoveDown(8)
        # -------------------------------------------------------------------- #
        # Player Two -> Normal input
        # -------------------------------------------------------------------- #
        if not self.switchTwo:
            if self.key[pygame.K_f]:
                self.pTwo.pTwoMoveUp(8)
            elif self.key[pygame.K_g]:
                self.pTwo.pTwoMoveDown(8)
        # -------------------------------------------------------------------- #
        # Switched input (Bonus effect)
        # -------------------------------------------------------------------- #
        else:
            # Remaining time for switch effect
            self.switchTwoBonusTime +=1
            if self.switchTwoBonusTime == 300:
                self.switchTwo = False
                self.switchTwoBonusTime = 0
            # New binds
            if self.key[pygame.K_g]:
                self.pTwo.pTwoMoveUp(8)
            elif self.key[pygame.K_f]:
                self.pTwo.pTwoMoveDown(8)
        # -------------------------------------------------------------------- #
        # Play again
        # -------------------------------------------------------------------- #
        if self.gameOver:
            if self.key[pygame.K_SPACE]:
                self.pOne.score = 0
                self.pTwo.score = 0
                self.gameOver = False
        # Keep watching
        return True

    # ------------------------------------------------------------------------ #
    # Draw score
    # ------------------------------------------------------------------------ #
    def score(self):
        # -------------------------------------------------------------------- #
        # Scoring
        # -------------------------------------------------------------------- #
        # Player One misses
        if self.ball.ballX < 0-self.ball.size:
            self.pTwo.score +=1
            self.ball.newBall(self.ball.dirX)
            self.speed = False
            self.velocity=1
        # Player Two misses
        elif self.ball.ballX > WIDTH:
            self.pOne.score +=1
            self.ball.newBall(-self.ball.dirX)
            self.speed = False
            self.velocity=1
        # Draw score
        scoreOne = self.font.render(str(self.pOne.score), True, WHITE)
        self.screen.blit(scoreOne, [WIDTH/2/2, 20])
        scoreTwo = self.font.render(str(self.pTwo.score), True, WHITE)
        self.screen.blit(scoreTwo, [WIDTH/2 + WIDTH/2/2, 20])
        # -------------------------------------------------------------------- #
        # Game Over
        # -------------------------------------------------------------------- #
        if self.pOne.score == 10 or self.pTwo.score == 10:
            self.firstGame = False
            self.gameOver = True
            self.switchOne = False
            self.switchTwo = False
            self.upOne = False
            self.upTwo = False
            self.downOne = False
            self.downTwo = False
            self.pOne.resize(1)
            self.pTwo.resize(1)

    # ------------------------------------------------------------------------ #
    # Fill screen
    # ------------------------------------------------------------------------ #
    def draw(self):#, screen):
        # Background
        self.screen.fill(BLACK)
        # Player One
        pygame.draw.rect(self.screen, WHITE, pygame.Rect(self.pOne.x,self.pOne.y+self.pOne.yDif,10,self.pOne.size))
        # Player Two
        pygame.draw.rect(self.screen, WHITE, pygame.Rect(self.pTwo.x,self.pTwo.y+self.pTwo.yDif,10,self.pTwo.size))
        # Ball
        if not self.gameOver:
            pygame.draw.circle(self.screen, WHITE, (int(self.ball.ballX),int(self.ball.ballY)),int(self.ball.size),0)

        # -------------------------------------------------------------------- #
        # Call the score() function and draw middle separator
        # -------------------------------------------------------------------- #
        self.score()
        for y in range (0,HEIGHT, self.middleSpace):
            pygame.draw.rect(self.screen, WHITE, pygame.Rect(self.middleX,y,10,10))

        # -------------------------------------------------------------------- #
        # Play again
        # -------------------------------------------------------------------- #
        if self.gameOver:
            if self.firstGame:
                endGame = self.font.render("NEO PONG", True, WHITE)
                self.screen.blit(endGame, [WIDTH/2-240, HEIGHT/3])
                replay = self.font.render("Press Start Button", True, WHITE)
                self.screen.blit(replay, [WIDTH/2+60, HEIGHT/3])
            else:
                endGame = self.font.render("Game Over", True, WHITE)
                self.screen.blit(endGame, [WIDTH/2-240, HEIGHT/3])
                replay = self.font.render("Play again?", True, WHITE)
                self.screen.blit(replay, [WIDTH/2+60, HEIGHT/3])

        # -------------------------------------------------------------------- #
        # Moving ball after bonus effects
        # -------------------------------------------------------------------- #
        if not self.gameOver:
            # ---------------------------------------------------------------- #
            # Speed Bonus
            # ---------------------------------------------------------------- #
            if self.speed:
                self.velocity = 3
                self.speedBonusTime +=1
                if self.speedBonusTime == 200:
                    self.speed = False
                    self.speedBonusTime = 0
                    self.velocity=1
            # ---------------------------------------------------------------- #
            # UpOne or DownOne Bonus
            # ---------------------------------------------------------------- #
            if self.upOne or self.downOne:
                if self.upOne:
                    self.upOneBonusTime +=1
                    if self.upOneBonusTime == 600:
                        self.upOneBonusTime = 0
                        self.pOne.resize(0.5)
                        self.upOne = False

                if self.downOne:
                    self.downOneBonusTime +=1
                    if self.downOneBonusTime == 600:
                        self.downOneBonusTime = 0
                        self.pOne.resize(2)
                        self.downOne = False
            # ---------------------------------------------------------------- #
            # UpTwo or DownTwo Bonus
            # ---------------------------------------------------------------- #
            if self.upTwo or self.downTwo:
                if self.upTwo:
                    self.upTwoBonusTime +=1
                    if self.upTwoBonusTime == 600:
                        self.upTwoBonusTime = 0
                        self.pTwo.resize(0.5)
                        self.upTwo = False

                if self.downTwo:
                    self.downTwoBonusTime +=1
                    if self.downTwoBonusTime == 600:
                        self.downTwoBonusTime = 0
                        self.pTwo.resize(2)
                        self.downTwo = False
            # ---------------------------------------------------------------- #
            # So, moving ball
            # ---------------------------------------------------------------- #
            self.ball.moveBall(self.pOne,self.pTwo,self.velocity)
            # ---------------------------------------------------------------- #
            # Bonus Time! (Timing before a new bonus)
            # ---------------------------------------------------------------- #
            self.timer +=1
            # Display Time!
            #time = self.font.render(str(self.timer), True, BLACK)
            #self.screen.blit(time, [WIDTH/2, 20])
            if self.timer == 400:
                self.timer = 0
                # Check if the game is over
                if not self.gameOver:
                    self.bonus.initBonus()
                    self.goBonus = True
            # ---------------------------------------------------------------- #
            # New bonus
            # ---------------------------------------------------------------- #
            if self.goBonus == True:
                self.bonus.new()
                self.screen.blit(self.bonus.bonusImg, (self.bonus.bonusX + self.bonus.bonusDirX, self.bonus.bonusY))
                # ------------------------------------------------------------ #
                # No bonus (Nobody hits)
                # ------------------------------------------------------------ #
                if not 0 < self.bonus.bonusX < WIDTH:
                    self.goBonus = False
                    self.direction = 0
                    self.bonus.destroy()
                # ------------------------------------------------------------ #
                # Player 1 bonus
                # ------------------------------------------------------------ #
                if self.bonus.bonusX <= 37:
                    if self.bonus.bonusY >= self.pOne.y + self.pOne.yDif -30:
                        if self.bonus.bonusY <= self.pOne.y + self.pOne.yDif + self.pOne.size:
                                self.goBonus = False
                                self.direction = 0
                                self.bonus.destroy()
                                # Speed Effect
                                if self.bonus.bonus == "speed":
                                    if self.speedBonusTime != 0:
                                        self.speedBonusTime = 0
                                    self.speed = True
                                # Switch Effect (Gift for Player Two)
                                elif self.bonus.bonus == "switch":
                                    if self.switchTwoBonusTime != 0:
                                        self.switchTwoBonusTime = 0
                                    self.switchTwo = True
                                # Up Effect
                                elif self.bonus.bonus == "up":
                                    if self.downOne:
                                        self.downOne = False
                                        self.pOne.resize(1)
                                    else:
                                        self.upOneBonusTime = 0
                                        self.upOne = True
                                        self.pOne.resize(2)
                                # Down Effect (Gift for Player Two)
                                elif self.bonus.bonus == "down":
                                    if self.upTwo:
                                        self.upTwo = False
                                        self.pTwo.resize(1)
                                    else:
                                        self.downTwoBonusTime = 0
                                        self.downTwo = True
                                        self.pTwo.resize(0.5)
                # ------------------------------------------------------------ #
                # Player 2 bonus
                # ------------------------------------------------------------ #
                if self.bonus.bonusX >= self.pTwo.x - 36:
                    if self.bonus.bonusY >= self.pTwo.y + self.pTwo.yDif -30:
                        if self.bonus.bonusY <= self.pTwo.y + self.pTwo.yDif + self.pTwo.size:
                                self.goBonus = False
                                self.direction = 0
                                self.bonus.destroy()
                                # Speed Effect
                                if self.bonus.bonus == "speed":
                                    if self.speedBonusTime != 0:
                                        self.speedBonusTime = 0
                                    self.speed = True
                                # Switch Effect (Gift for Player One)
                                elif self.bonus.bonus == "switch":
                                    if self.switchOneBonusTime != 0:
                                        self.switchOneBonusTime = 0
                                    self.switchOne = True
                                # Up Effect
                                elif self.bonus.bonus == "up":
                                    if self.downTwo:
                                        self.downTwo = False
                                        self.pTwo.resize(1)
                                    else:
                                        self.upTwoBonusTime = 0
                                        self.upTwo = True
                                        self.pTwo.resize(2)
                                # Down Effect (Gift for Player One)
                                elif self.bonus.bonus == "down":
                                    if self.upOne:
                                        self.upOne = False
                                        self.pOne.resize(1)
                                    else:
                                        self.downOneBonusTime = 0
                                        self.downOne = True
                                        self.pOne.resize(0.5)
