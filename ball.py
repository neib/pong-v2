import pygame
import random

# Get screen resolution
pygame.init()
infoRes = pygame.display.Info()
pygame.quit()
# Fullscreen
WIDTH = infoRes.current_w
HEIGHT = infoRes.current_h
# Manual set
#WIDTH = 1024
#HEIGHT = 768


# ---------------------------------------------------------------------------- #
# Ball
# ---------------------------------------------------------------------------- #
class Ball():
    def __init__(self):
        self.ballSound = pygame.mixer.Sound("sounds/ball.ogg")
        self.ballX = WIDTH/2 -15
        self.ballY = HEIGHT/2 -15
        # -------------------------------------------------------------------- #
        self.dirX = 3
        self.topDownAngle = 4
        self.middleAngle = 2
        self.delayTodDown = 100
        self.delayPlayers = 100
        # -------------------------------------------------------------------- #
        self.ballDirX = random.choice((self.dirX,-self.dirX))
        self.ballDirY = random.choice((self.topDownAngle ,-self.topDownAngle ,self.middleAngle,-self.middleAngle))
        self.size = 1*HEIGHT/100
        self.accelerationCoefficient = 1.08
        # Sound Bug ---------------------------------------------------------- #
        self.soundOne = True
        self.soundTwo = True
        self.soundTop = True
        self.soundDown = True
    # ------------------------------------------------------------------------ #
    # New Ball
    # ------------------------------------------------------------------------ #
    def newBall(self,x):
        self.ballX = WIDTH/2 -15
        self.ballY = HEIGHT/2 -15

        self.ballDirX = x
        self.ballDirY = random.choice((self.topDownAngle,-self.topDownAngle,self.middleAngle,-self.middleAngle))
    # ------------------------------------------------------------------------ #
    # Move ball
    # ------------------------------------------------------------------------ #
    def moveBall(self, pOne, pTwo, velocity=1):
        # Top Limit
        if self.ballY == self.size:
            self.ballDirY *= -1
            #self.ballSound.play()
        # Bottom Limit
        elif self.ballY == HEIGHT-self.size:
            self.ballDirY *= -1
            #self.ballSound.play()
        # ------------------------------------------------------------------------ #
        # Sound Bug Top/Down
        # ------------------------------------------------------------------------ #
        if self.ballY <= self.size +self.delayTodDown and self.ballDirY < 0:
            if self.soundTop:
                self.ballSound.play()
                self.soundTop = False
        elif self.ballY >= HEIGHT-self.size -self.delayTodDown and self.ballDirY > 0:
            if self.soundDown:
                self.ballSound.play()
                self.soundDown = False

        if self.ballY <= self.size +self.delayTodDown and self.ballDirY > 0:
            self.soundTop = True
        elif self.ballY >= HEIGHT-self.size -self.delayTodDown and self.ballDirY < 0:
            self.soundDown = True
        # ------------------------------------------------------------------------ #

        # -------------------------------------------------------------------- #
        # Give ball velocity (Speed Bonus)
        # -------------------------------------------------------------------- #
        self.ballX += self.ballDirX * velocity
        self.ballY += self.ballDirY * velocity

        # -------------------------------------------------------------------- #
        # Borders
        # -------------------------------------------------------------------- #
        self.ballY = min(self.ballY, HEIGHT-self.size)
        self.ballY = max(self.ballY, self.size)
        # -------------------------------------------------------------------- #
        # Player One colision
        # -------------------------------------------------------------------- #
        # Top
        if pOne.x <= self.ballX <= pOne.x+10 +self.size and self.ballY >= pOne.y + pOne.yDif -30 and self.ballY <= pOne.y + pOne.yDif +pOne.size/5 and self.ballDirX < 0:
            #self.ballSound.play()
            self.ballDirX *= -self.accelerationCoefficient
            if self.ballDirY > 0 :
                self.ballDirY = self.topDownAngle
            else:
                self.ballDirY = -self.topDownAngle
        # Middle - Top
        if pOne.x <= self.ballX <= pOne.x+10 +self.size and self.ballY >= pOne.y + pOne.yDif -30 +pOne.size/5 +1 and self.ballY <= pOne.y + pOne.yDif + pOne.size*2/5 and self.ballDirX < 0:
            #self.ballSound.play()
            self.ballDirX *= -self.accelerationCoefficient
            if self.ballDirY > 0 :
                self.ballDirY = self.middleAngle
            else:
                self.ballDirY = -self.middleAngle
        # Middle
        if pOne.x <= self.ballX <= pOne.x+10 +self.size and self.ballY >= pOne.y + pOne.yDif -30 +pOne.size*2/5 +1 and self.ballY <= pOne.y + pOne.yDif +pOne.size*3/5 and self.ballDirX < 0:
            #self.ballSound.play()
            self.ballDirX *= -self.accelerationCoefficient
            self.ballDirY *= 0
        # Middle - Down
        if pOne.x <= self.ballX <= pOne.x+10 +self.size and self.ballY >= pOne.y + pOne.yDif -30 +pOne.size*3/5 +1 and self.ballY <= pOne.y + pOne.yDif + pOne.size*4/5 and self.ballDirX < 0:
            #self.ballSound.play()
            self.ballDirX *= -self.accelerationCoefficient
            if self.ballDirY > 0 :
                self.ballDirY = self.middleAngle
            else:
                self.ballDirY = -self.middleAngle
        # Down
        if pOne.x <= self.ballX <= pOne.x+10 +self.size and self.ballY >= pOne.y + pOne.yDif -30 +pOne.size*4/5 +1 and self.ballY <= pOne.y + pOne.yDif + pOne.size and self.ballDirX < 0:
            #self.ballSound.play()
            self.ballDirX *= -self.accelerationCoefficient
            if self.ballDirY > 0 :
                self.ballDirY = self.topDownAngle
            else:
                self.ballDirY = -self.topDownAngle
        # -------------------------------------------------------------------- #
        # Player Two colision
        # -------------------------------------------------------------------- #
        # Top
        if pTwo.x+10 >= self.ballX >= pTwo.x  -self.size and self.ballY >= pTwo.y + pTwo.yDif -30 and self.ballY <= pTwo.y + pTwo.yDif +pTwo.size/5 and self.ballDirX > 0:
            #self.ballSound.play()
            self.ballDirX *= -self.accelerationCoefficient
            if self.ballDirY > 0 :
                self.ballDirY = self.topDownAngle
            else:
                self.ballDirY = -self.topDownAngle
        # Middle - Top
        if pTwo.x+10 >= self.ballX >= pTwo.x -self.size and self.ballY >= pTwo.y + pTwo.yDif -30 +pTwo.size/5 +1 and self.ballY <= pTwo.y + pTwo.yDif +pTwo.size*2/5 and self.ballDirX > 0:
            #self.ballSound.play()
            self.ballDirX *= -self.accelerationCoefficient
            if self.ballDirY > 0 :
                self.ballDirY = self.middleAngle
            else:
                self.ballDirY = -self.middleAngle
        # Middle
        if pTwo.x+10 >= self.ballX >= pTwo.x -self.size and self.ballY >= pTwo.y + pTwo.yDif -30 +pTwo.size*2/5 +1 and self.ballY <= pTwo.y + pTwo.yDif +pTwo.size*3/5 and self.ballDirX > 0:
            #self.ballSound.play()
            self.ballDirX *= -self.accelerationCoefficient
            self.ballDirY *= 0
        # Middle - Down
        if pTwo.x+10 >= self.ballX >= pTwo.x -self.size and self.ballY >= pTwo.y + pTwo.yDif -30 +pTwo.size*3/5 +1 and self.ballY <= pTwo.y + pTwo.yDif +pTwo.size*4/5 and self.ballDirX > 0:
            #self.ballSound.play()
            self.ballDirX *= -self.accelerationCoefficient
            if self.ballDirY > 0 :
                self.ballDirY = self.middleAngle
            else:
                self.ballDirY = -self.middleAngle
        # Down
        if pTwo.x+10 >= self.ballX >= pTwo.x -self.size and self.ballY >= pTwo.y + pTwo.yDif -30 +pTwo.size*4/5 +1 and self.ballY <= pTwo.y + pTwo.yDif +pTwo.size and self.ballDirX > 0:
            #self.ballSound.play()
            self.ballDirX *= -self.accelerationCoefficient
            if self.ballDirY > 0 :
                self.ballDirY = self.topDownAngle
            else:
                self.ballDirY = -self.topDownAngle


        # -------------------------------------------------------------------- #
        # Sound Bug Players
        # -------------------------------------------------------------------- #
        # Player One
        if self.ballX <= pOne.x+10+self.delayPlayers +self.size and self.ballY >= pOne.y + pOne.yDif -30 and self.ballY <= pOne.y + pOne.yDif +pOne.size and self.ballDirX < 0:
            if self.soundOne:
                self.ballSound.play()
                self.soundOne = False
        if self.ballX <= pOne.x+10+self.delayPlayers +self.size and self.ballY >= pOne.y + pOne.yDif -30 and self.ballY <= pOne.y + pOne.yDif +pOne.size and self.ballDirX > 0:
            self.soundOne = True
        # Player Two
        if self.ballX >= pTwo.x-self.delayPlayers  -self.size and self.ballY >= pTwo.y + pTwo.yDif -30 and self.ballY <= pTwo.y + pTwo.yDif +pTwo.size and self.ballDirX > 0:
            if self.soundTwo:
                self.ballSound.play()
                self.soundTwo = False
        if self.ballX >= pTwo.x-self.delayPlayers  -self.size and self.ballY >= pTwo.y + pTwo.yDif -30 and self.ballY <= pTwo.y + pTwo.yDif +pTwo.size and self.ballDirX < 0:
            self.soundTwo = True
        # ------------------------------------------------------------------------ #
