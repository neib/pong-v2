#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pygame
from game import Game

pygame.init()
# Get screen resolution
infoRes = pygame.display.Info()
# Fullscreen
WIDTH = infoRes.current_w
HEIGHT = infoRes.current_h
# Manual set
#WIDTH = 1024
#HEIGHT = 768

#screen = pygame.display.set_mode((WIDTH, HEIGHT))
screen = pygame.display.set_mode((WIDTH, HEIGHT), pygame.FULLSCREEN)

# Title, icon
pygame.display.set_caption("Neo P0NG")
#icon = pygame.image.load("images/icon.png")
#pygame.display.set_icon(icon)

# Hide mouse cursor
pygame.mouse.set_visible(False)

# Load the game functions
game = Game(screen)

# Display
running = True
while running:
    running = game.events()
    game.draw()
    pygame.display.update()

pygame.quit()
