import pygame

# Get screen resolution
pygame.init()
infoRes = pygame.display.Info()
pygame.quit()
# Fullscreen
WIDTH = infoRes.current_w
HEIGHT = infoRes.current_h
# Manual set
#WIDTH = 1024
#HEIGHT = 768


# ---------------------------------------------------------------------------- #
# Player one
# ---------------------------------------------------------------------------- #
class PlayerOne():
    def __init__(self):
        self.size = (10*HEIGHT/100)
        self.x = 30
        self.y = HEIGHT/2 -(self.size/2)
        self.yDif = 0
        self.score = 0
        # Changes the bottom collision according to the size of the player
        self.bottomLimit = 0
    # ------------------------------------------------------------------------ #
    # Moves
    # ------------------------------------------------------------------------ #
    def pOneMoveUp(self,y):
        if self.yDif-y >= -self.y+10:
            self.yDif -= y
    def pOneMoveDown(self,y):
        if self.yDif+y <= self.y-10 -self.bottomLimit:
            self.yDif += y
    # ------------------------------------------------------------------------ #
    # Get size
    # ------------------------------------------------------------------------ #
    def resize(self,s):
        if (10*HEIGHT/100)*0.5 <= self.size <= (10*HEIGHT/100)*2:
            if not (self.size == (10*HEIGHT/100)*0.5 and s == 0.5):
                if not (self.size == (10*HEIGHT/100)*2 and s == 2):
                    if s == 0.5:
                        self.bottomLimit-=self.size/2
                        self.size*=s
                    elif s == 2:
                        self.bottomLimit+=self.size
                        self.size*=s
                    elif s == 1:
                        self.bottomLimit = 0
                        self.size = (10*HEIGHT/100)
        if self.yDif > self.y-10 -self.bottomLimit:
            self.yDif = self.y-10 -self.bottomLimit

# ---------------------------------------------------------------------------- #
# Player Two
# ---------------------------------------------------------------------------- #
class PlayerTwo():
    def __init__(self):
        self.size = (10*HEIGHT/100)
        self.x = WIDTH - 10 - 30 # 10 == Player width
        self.y = HEIGHT/2 -(self.size/2)
        self.yDif = 0
        self.score = 0
        # Changes the bottom collision according to the size of the player
        self.bottomLimit = 0
    # ------------------------------------------------------------------------ #
    # Moves
    # ------------------------------------------------------------------------ #
    def pTwoMoveUp(self,y):
        if self.yDif-y >= -self.y+10:
            self.yDif -= y
    def pTwoMoveDown(self,y):
        if self.yDif+y <= self.y-10 -self.bottomLimit:
            self.yDif += y
    # ------------------------------------------------------------------------ #
    # Get size
    # ------------------------------------------------------------------------ #
    def resize(self,s):
        if (10*HEIGHT/100)*0.5 <= self.size <= (10*HEIGHT/100)*2:
            if not (self.size == (10*HEIGHT/100)*0.5 and s == 0.5):
                if not (self.size == (10*HEIGHT/100)*2 and s == 2):
                    if s == 0.5:
                        self.bottomLimit-=self.size/2
                        self.size*=s
                    elif s == 2:
                        self.bottomLimit+=self.size
                        self.size*=s
                    elif s == 1:
                        self.bottomLimit = 0
                        self.size = (10*HEIGHT/100)
        if self.yDif > self.y-10 -self.bottomLimit:
            self.yDif = self.y-10 -self.bottomLimit
